var dirty1 = false;
var dirty2 = false;
var dirty3 = false;
var dirty4 = false;
var db = null;
var updated = false;

var b2ot, b3ot;
var tsz = .045;
var tesz = .066;
var bsz = .125;
var losz = .075;
var uc = 0;
var uspop = 324459463;

/** nearest cent roundup */
function nc(d) {
    return Math.round(d * 100) / 100;
}

/** nearest cent string */
function ncs(d) {
	// return nc(d).toString();
    return nc(d).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    });
}

function txtpa(txt) {
   let re =/([+\-]?\s*([0-9,]*)([\.][0-9]*)?)/g;
   // let texp = "1.1215 +-++- a 3.45 67 44 - 6.1234";
   let res1 = txt.match(re);
   if(res1==null) res1=[];
   return res1.map((s)=>{
     s=s.replace(/\s+/g,'')
     s=s.replace(/,/g,'')
     return parseFloat(s);
   }).filter((n)=>{
      return n != null && Number.isFinite(n);
   }); 
}

function txtp(txt) {
    return txtpa(txt).reduce((a,c)=>{ return a+c; },0);
}


function sql(stmnt, dat) {
    if (db == null) {
        db = app.OpenDatabase(dbfn);
        db.ExecuteSql("PRAGMA synchronous = OFF");
    }
    return new Promise((res, err) => {
        db.ExecuteSql(stmnt, dat, res, err);
    }).catch((err) => {
        app.Alert(err + ','+stmnt+','+dat); // for debugging
        // throw err;
    });
}

async function calc5() {
	let res1 = davg.num - dallow.num;
	inec.te.SetText(ncs(res1*div));
}

async function calc4() {
	let tf = cashb.num + fsb.num + dxb.num;
	let tl = tf / davg.num;
	let tmp = ncs(tl);
	tleft.te.SetText(tmp);
}

async function calc3() {
    let res = await sql("SELECT * FROM daily WHERE 'ts' IS NOT NULL ORDER BY 'ts' ASC;");
    let rc = res.rows.length;
    sc = 2*7;
    bd = new Date(res.rows.item(rc-sc-1).ts);
    ed = new Date(res.rows.item(rc-1).ts);
    dd = (ed.getTime() - bd.getTime()) / (1000*60*60*24);
    app.ShowPopup(bd + " " + ed + " " + dd + " days");
    var te = 0;
    for (i = rc-sc-1; i < rc; i++) {
        let r = res.rows.item(i);
        te += r.cash_spent;
        te += r.fs_spent;
        te += r.dx_spent;
        if(i==rc-1) {
            let tmp = ncs(r.cash_spent + r.fs_spent + r.dx_spent);
            dtot.te.SetText(tmp);
            dtot.lbl.SetText(r.ts);
        }
    }
    let tmp = ncs(te / dd);
    davg.te.SetText(tmp);
}

function d2t(dv) {
	let d = Math.floor(dv);
	let h = Math.floor(dv * 24 % 24);
	let m = Math.round(dv * (24*60) % (60));
	return d + "d," + h + "h," + m + "m";
}

var div;

/** using min arrival day calc daily spending limit */
async function calc2() {
    let fsd = dl(18);
    let dxd = dl(28);
    div = Math.max(1, Math.min(fsd, dxd));
    let da = (cashb.num +  dxb.num) / div;
    let tmp = ncs(da);
    dallow.te.SetText(tmp);
    app.ShowPopup(d2t(fsd) + " " + d2t(dxd));
}

/** fractional days before monthly income */
function dl(dom) {
    let cd = new Date();
    // let at = cd + "\n";

    let nd = new Date();
    let ms = Math.round(dom * (24*60*60) % (60));
    let mm = Math.floor(dom * (24*60) % (60));
    let mh = Math.floor(dom * 24 % (24));
    // at += mh + ":" + mm + ":" + ms + "\n";
    nd.setSeconds(ms);
    nd.setMinutes(mm);
    nd.setHours(mh);
    nd.setDate(Math.floor(dom));

    let em;
    let md = cd.getDate();
    if(cd>=nd) {
        nd.setMonth(nd.getMonth()+1);
    }
    // at += nd + "\n";
    let t1 = cd.getTime();
    // at += t1 + "\n";
    let t2 = nd.getTime();
    // at += t2 + "\n";
    let days = (t2 - t1) / (8.64e7);
    // at += days;
    // app.Alert(at);
    return days;
}


/** table_exists */
async function tblexists(nm) {
    let res = await sql("SELECT COUNT() AS cnt FROM sqlite_master WHERE type='table' AND name=?",[nm]);
    return res.rows.item(0).cnt > 0;
}

/** income_expense table exists */
async function iee() {
    return await tblexists("inc_exp");
}

/** 'daily' table exists */
async function de() {
    return await tblexists("daily");
}

/** 'monthly' table exists */
async function me() {
    return await tblexists("monthly");
}

var mtbl_cs ="CREATE TABLE monthly (ts TIMESTAMP PRIMARY KEY,cash_recd REAL,fs_recd REAL,dx_recd REAL,cash_spent REAL,fs_spent REAL,dx_spent REAL)";

var dtbl_cs = "CREATE TABLE daily (ts TIMESTAMP PRIMARY KEY,cash_recd REAL,fs_recd REAL,dx_recd REAL,cash_spent REAL,fs_spent REAL,dx_spent REAL)";

var ietbl_cs = "CREATE TABLE inc_exp (bts TIMESTAMP NOT NULL,ets TIMESTAMP NOT NULL PRIMARY KEY,cash_recd REAL,fs_recd REAL,dx_recd REAL,cash_spent REAL,fs_spent REAL,dx_spent REAL)";

/** create monthly table if missing */
async function mtbl() {
    let mex = await me();
    if (mex && dirty4) {
        // await sql("DELETE FROM monthly");
    } else {
        if (!mex) {
            await sql(mtbl_cs);
            dirty4 = true;
        }
        // debug
        // dirty4 = true;
    }
    return mex;
}

/** create daily table if missing */
async function dtbl() {
    let dex = await de();
    if (dex && dirty3) {
        // await sql("DELETE FROM daily");
    } else {
        if (!dex) {
            await sql(dtbl_cs);
            dirty3 = true;
        }
        // debug
        // dirty3 = true;
    }
    return dex;
}

/** create income_expense table if missing */
async function ietbl() {
    let iex = await iee();
    if (iex && dirty2) {
        // await sql("DELETE FROM inc_exp");
    } else {
        if (!iex) {
            await sql(ietbl_cs);
            dirty2 = true;
        }
        // debug
        // dirty2=true;
    }
    return iex;
}

async function lts() {
    let res = await sql("SELECT ts FROM currentc ORDER BY ts DESC");
    return res.rows.item(uc).ts;
}

/** updates i/e and daily summary tables */
async function update_ietbl() {
    app.ShowProgressBar("updating tables...");
    let pd = await lts();
    let iex = await ietbl();
    let wh = "";
    if (iex && !rcb.GetChecked()) {
        wh = " WHERE ts>='" + pd + "'";
    }
    if (dirty2 || rcb.GetChecked()) {
        let ss = "SELECT ts, cash, fs, dx FROM currentc" + wh + " ORDER BY ts";
        let res = await sql(ss);
        let rc = res.rows.length;
        let j = 0;
        let i = 1;
        // await sql("BEGIN TRANSACTION");
        await db.transaction(function(tx) {
            for (; i < rc; i++) {
                let pr = res.rows.item(j);
                let cr = res.rows.item(i);
                let bts = pr.ts;
                let ets = cr.ts;
                let cash_diff = nc(cr.cash - pr.cash);
                let fs_diff = nc(cr.fs - pr.fs);
                let dx_diff = nc(cr.dx - pr.dx);
                let cash_recd = cash_diff > 0 ? (cash_diff) : null;
                let fs_recd = fs_diff > 0 ? (fs_diff) : null;
                let dx_recd = dx_diff > 0 ? (dx_diff) : null;
                let cash_spent = cash_diff < 0 ? nc(-cash_diff) : null;
                let fs_spent = fs_diff < 0 ? nc(-fs_diff) : null;
                let dx_spent = dx_diff < 0 ? nc(-dx_diff) : null;
                if (cash_diff != 0 || fs_diff != 0 || dx_diff != 0) {
                   let res2 = tx.executeSql("INSERT OR REPLACE INTO inc_exp (bts, ets, cash_recd, fs_recd, dx_recd, cash_spent, fs_spent, dx_spent) VALUES (?,?,?,?,?,?,?,?)",
                       [bts, ets, cash_recd, fs_recd, dx_recd, cash_spent, fs_spent, dx_spent]);
                   //if(res2 && res.rows.length>0) {
                   //    app.Alert(res2.rows.item(0));
                   //}
                   j = i;
                   dirty3 = true;
               }
               uc--;
               app.UpdateProgressBar(i * 80 / rc);
            }
        });
        dirty2 = false;
        // await sql("COMMIT TRANSACTION");
    }
    app.UpdateProgressBar(80);
    // let ws = "substr(datetime(ets,'localtime'), 1, 10)";
    let dex = await dtbl();
    wh = "";
    if (dex && !rcb.GetChecked()) {
        wh = " WHERE substr(datetime(ets,'localtime'), 1, 10)>='" +
            new Date(pd).toLocaleString("en-CA").substring(0, 10) + "'";
    }
    if (dirty3 || rcb.GetChecked()) {
        let sst = " SELECT substr(datetime(ets,'localtime'), 1, 10) AS ts, SUM(cash_recd) AS cash_recd, SUM(fs_recd) AS fs_recd, SUM(dx_recd) AS dx_recd, SUM(cash_spent) AS cash_spent, SUM(fs_spent) AS fs_spent, SUM(dx_spent) AS dx_spent FROM inc_exp" +
            wh +
            " GROUP BY substr(datetime(ets,'localtime'), 1, 10)";
        // await sql("BEGIN TRANSACTION");
        await sql("INSERT OR REPLACE INTO daily" + sst);
        // await sql("COMMIT TRANSACTION");
        dirty3 = false;
        dirty4 = true;
    }

///--
    app.UpdateProgressBar(90);
    let mex = await mtbl();
    wh = "";
    if (mex && !rcb.GetChecked()) {
        wh = " WHERE substr(datetime(ets,'localtime'), 1, 7)>='" + 
            new Date(pd).toLocaleString("en-CA").substring(0, 7) + "'";
    }
    if (dirty4 || rcb.GetChecked()) {
        let sst = " SELECT substr(datetime(ets,'localtime'), 1, 7) AS ts, SUM(cash_recd) AS cash_recd, SUM(fs_recd) AS fs_recd, SUM(dx_recd) AS dx_recd, SUM(cash_spent) AS cash_spent, SUM(fs_spent) AS fs_spent, SUM(dx_spent) AS dx_spent FROM inc_exp" +
            wh +
            " GROUP BY substr(datetime(ets,'localtime'), 1, 7)";
        // await sql("BEGIN TRANSACTION");
        await sql("INSERT OR REPLACE INTO monthly" + sst);
        // await sql("COMMIT TRANSACTION");
        dirty4 = false;
    }

///--
    app.UpdateProgressBar(100);
    app.HideProgressBar();
    if(rcb.GetChecked()) {
        uc=0;
        rcb.SetChecked(false);
    }
}

/** adds balance update */
async function log_balances() {
    await sql("INSERT INTO currentc (ts, cash, fs, dx) VALUES (?,?,?,?)", [new Date().toISOString(),cashb.num, fsb.num, dxb.num]);
    dirty1 = false;
    dirty2 = true;
    uc++;
}

async function SaveNumber(name, num) {
    await sql("UPDATE nums SET (num,ts)=(?,?) WHERE name=?", [num, new Date().toISOString(), name]);
}

function loadnums() {
    db.transaction(function(tx) {
        function load(mv) {
            tx.executeSql("SELECT num FROM nums WHERE name=?", [mv.name], 
                (t, res)=>{
                    if(res) {
                        if(res.rows.length!=1) { alert(mv.name); }
                        mv.num = res.rows.item(0).num;
                        mv.te.SetText(mv.toString());
                    }
                }, (t, e)=>{ alert(e); });
        }
        load(cashb);
        load(fsb);
        load(dxb);
        load(davg);
        load(dallow);
        load(inec);
        load(dtot);
        load(tleft);
    });
}

function teot() {
    let v = this.mvar;
    app.ShowPopup(" v.name: " + v.name);
}

function sc(o) {
	try {
        o.SetBackColor( "white" );
    } catch(e) {
    }
    try {
        o.SetTextColor( "black" );
    } catch(e) {
    }
}

class Mvar {
    constructor(n,ne) {
        this.num = 0;
        this.te = app.CreateTextEdit("", .5, tesz,"singleline"+(ne!==""?","+ne:""));
        this.name = n;
        this.te.SetHint(n);
        this.te.mvar = this; // parent
        
        this.lbl = app.CreateText(this.name, .325, tsz,"right");
        this.lo = app.CreateLayout("linear", "horizontal");
        this.lo.AddChild(this.lbl);
        this.lo.AddChild(this.te);
        this.lo.SetSize(.8, tesz);
        this.cf = async function () {
            let res2,res3;
            let res1 = this.GetText();
            res2 = txtpa(res1);
            res3 = res2.reduce((a,c)=>{ return a+c; },0);
            if(res2.length==0) {
            }
            app.ShowPopup(
                this.mvar.name + ": " + res1 + " = " + ncs(res3)
            );
        };
        this.te.SetOnTouch(this.cf);
        this.te.SetOnChange(this.cf);
        sc(this.te);
        sc(this.lbl);
        sc(this.lo);
    }
    async save() {
        let txt = this.te.GetText();
        let ia = txtpa(txt);
        let acc = 0;
        for(let ci of ia) {
            acc = nc(acc + ci);
            await this.newbal(acc);
        }
        this.te.SetText(this.toString());
    }
    async newbal(nb) {
        if(this.num !== nb) {
            this.num = nb;
            await this.te.SetText(this.toString());
            await SaveNumber(this.name, this.num);
            if(this.isbal) await log_balances();
        }
    }
    toString() {
        return ncs(this.num);
    }
}

var cashb, fsb, dxb,dtot,davg,dallow,tleft,inec;
var lts;
var rcb;


// app.LoadScript(app.GetPath()+"/functions.js");
async function slow(f) {
   app.ShowProgress();
   await f();
   app.HideProgress();
}

async function OnStart() {
    dbinstall();
    db = app.OpenDatabase(dbfn);
    cashb = new Mvar("Cash","");
    fsb = new Mvar("FS","");
    dxb = new Mvar("DX","");
    cashb.isbal = true;
    fsb.isbal = true;
    dxb.isbal = true;
    davg = new Mvar("DAvgExp","readonly,nokeyboard"); davg.te.SetTextColor("turquoise");
    dallow = new Mvar("DAllow","readonly,nokeyboard"); dallow.te.SetTextColor("fuchsia");
    inec = new Mvar("INec","readonly,nokeyboard"); inec.te.SetTextColor("green");
    dtot = new Mvar("DTotExp","readonly,nokeyboard"); dtot.te.SetTextColor("blue");
    tleft = new Mvar("TLeft","readonly,nokeyboard"); tleft.te.SetTextColor("red");
    loadnums();
    
    let b1 = app.CreateButton("Update", .5, bsz);
    b1.SetOnTouch(
    async () => {
        await slow(async ()=>{
            await cashb.save();
            await fsb.save();
            await dxb.save();
            await update_ietbl();
        });
        await b3ot();
    });
    // sc(b1);
    
    let b3 = app.CreateButton("Recalc",.5, bsz);
    b3ot =
    async ()=>{
        await calc3(); // avg daily exp
        await calc2(); // amnt per day left
        await calc4();
        await calc5();
        await davg.save();
        await dallow.save();
        await inec.save();
        await dtot.save();
        await tleft.save();
    };
    b3.SetOnTouch(b3ot);
    // sc(b3);
    
    let b2 = app.CreateButton("Exit",.5, bsz);
    b2ot =
    async () => {
        // dlg.Hide();
        // dlg.Dismiss();
        // dlg.Destroy();
        // dlg.Release();
        // await b3ot();
        if (db != null) {
            db.Close(); 
            db = null;
        }
        dbbackup();
        app.Exit();
    };
    b2.SetOnTouch(b2ot);
    // sc(b2);
    window.onclose = b2ot;
    rcb = app.CreateCheckBox("Regen i/e tables");
    sc(rcb);
 
    let ttl1 = app.CreateText("Balance Log");
    ttl1.SetTextSize(26);
    // sc(ttl1);
    
    let lyo1 = app.CreateLayout("linear", "vertical, center");
    lyo1.AddChild(cashb.lo);
    lyo1.AddChild(fsb.lo);
    lyo1.AddChild(dxb.lo);
    sc(lyo1);
            
    let lyo2 = app.CreateLayout("linear","vertical,center");
    lyo2.AddChild(davg.lo);
    lyo2.AddChild(dallow.lo);
    lyo2.AddChild(inec.lo);
    lyo2.AddChild(dtot.lo);
    lyo2.AddChild(tleft.lo);

    sc(lyo2);
        
    let lyo0 = app.CreateLayout("linear", "vertical, center");
    lyo0.AddChild(ttl1);
    lyo0.AddChild(lyo1);
    lyo0.AddChild(lyo2);
    lyo0.AddChild(b1);
    lyo0.AddChild(b3);
    lyo0.AddChild(b2);
    lyo0.AddChild(rcb);
    sc(lyo0);
    lyo0.SetSize(1,1);
    app.AddLayout(lyo0);
}

/** various old csv imports */
async function ib() {
    let ft = app.ReadFile(app.GetPath() + "/blog2.csv");
    let lna = ft.split('\n');
    for (i = 0; i < lna.length; i++) {
        let f = lna[i].split(',');

        let tp = f[1].split(':');
        if (Number(tp[0]) < 10) tp[0] = '0' + Number(tp[0]);
        f[1] = tp.join(':');

        let ts = f[0] + " " + f[1];
        let res = await sql(
            "INSERT INTO currentc (ts, cash, fs, dx) VALUES (?,?,?,?)",
            [new Date(ts).toISOString(), f[2], f[3], f[4]]
        );
    }
    app.Exit();
}

/** regenerates currentc table restoring struct preserving data */
async function fixtbl() {
    let sqlt1 = "CREATE TABLE IF NOT EXISTS tmpc (ts TIMESTAMP PRIMARY KEY NOT NULL,cash REAL,fs REAL,dx REAL)";
    //let sqlt2 = "INSERT INTO tmpc SELECT * FROM currentc ORDER BY ts";
    //let sqlt3 = "DROP TABLE currentc";
    //let sqlt4 = "ALTER TABLE tmpc RENAME TO currentc";
    await sql(sqlt1);
    await sql("DELETE FROM tmpc");
    let res = await sql("select * from currentc order by ts");
    for(let i=0;i<res.rows.length;i++) {
        let row = res.rows.item(i);
        await sql("insert into tmpc (ts,cash,fs,dx) values (?,?,?,?)",
            [new Date(row.ts).toISOString(),row.cash,row.fs,row.dx]);
    }
    //await sql(sqlt2);
    //await sql(sqlt3);
    //await sql(sqlt4);
    app.Exit();
}


var dbfn = "Finance.db";
    
var fn1 = app.GetPrivateFolder("") + "/../databases";
if(fn1.length != 0) fn1+="/";
fn1 += dbfn;

var fn2 = "/sdcard/Documents";
if(fn2.length != 0) fn2 += "/";
fn2 += dbfn;

function dbinstall() {
	try {
        // app.DeleteFile(fn1);
	    var fe1 = app.FileExists(fn1);
        // if(fe1) throw new Error("db " + fn1 + " didn't delete");
	    var fe2 = app.FileExists(fn2);
	    if(!fe2) throw new Error("backup db " + fn2 + " not found.");
	    if(!fe1 && fe2) {
		    // app.Alert("copy " + fn2 + " to \n" + fn1 + " (exists)");
		    app.CopyFile(fn2, fn1);
            fe1 = app.FileExists(fn1);
            if(!fe1) throw new Error("file " + fn2 + " didn't copy");
	    } else
	    if(fe1 && fe2) {
		    var d1 = app.GetFileDate(fn1);
		    var d2 = app.GetFileDate(fn2);
		    if(d2>d1) {
			    // app.Alert("copy " + fn2 + " to \n" + fn1 + " (date)");
	            app.CopyFile(fn2,fn1);
                fe1 = app.FileExists(fn1);
                if(!fe1) throw new Error("file " + fn1 + " didn't copy");
	        }
	    }
        else {
            throw new Error("one or more files missing");
        }
	} catch(e) {
		app.Alert(e);
	}
}

function dbbackup() {
	try {
        var fe1 = app.FileExists(fn1);
	    var fe2 = app.FileExists(fn2);
	    if(fe1 && !fe2) {
		    // app.Alert("copy " + fn1 + " to \n" + fn2 + " (exists)");
		    app.CopyFile(fn1,fn2);
            var fe2 = app.FileExists(fn2);
	        if(!fe2) throw new Error("file " + fn2 + " didn't arrive");
	     } else
        if(fe1 && fe2) {
	        var d1 = app.GetFileDate(fn1);
	        var d2 = app.GetFileDate(fn2);
	        if(d1>d2) {
		        // app.Alert("copy " + fn1 + " to \n" + fn2 + " (date)");
		        app.CopyFile(fn1,fn2);
                var fe2 = app.FileExists(fn2);
	            if(!fe2) throw new Error("file " + fn2 + " is now missing");
                d2 = app.GetFileDate(fn2);
                if(d1>d2) throw new Errorr("file " + fn2 + " didn't copy");
	        }
        }
    }
    catch(e) {
		app.Alert(e);
	}
}