# If not running interactively, don't do anything
case $- in
    *i*) ;;
    *) return ;;
esac

export PS1='$PWD\n\$ '
export HISTCONTROL=ignoreboth:erasedups

export app=/data/data/com.termux/files

alias ab='cd /sdcard/projects/devbak; python inot.py &'
alias netup='main.pl netup'

alias pk='pka.sh'
alias pke='pka.sh; exit'

ps -o pid,comm
alias ps='ps -o pid,comm'
alias start=start.sh
source ~/.bashrc0
source ~/secret.sh
